# SFDX setup and pulling changes to a sample scratch org

## Steps to follow to create and setup a scratch org with exisitng configurations:



1. Enable DevHub Access - **Setup** > Search **DevHub** on the left panel > **Enable** DevHub Access

2. Download sfdx package here : https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_install_cli.htm#sfdx_setup_install_cli_macos 

3. Download the Visual Studio Salesforce CLI extension package here : https://marketplace.visualstudio.com/items?itemName=salesforce.salesforcedx-vscode

4. Setup the scratch org locally

    ```
    git clone git@bitbucket.org:ashishvashishth/salesforce.git
    git checkout feature/Shampa_sfdx
    git pull
    ```

5. Go to the code folder

    ```
    cd salesforce
    ```

6. Test if the sfdx package is installed

    ```
    sfdx update
    ```

7. Login to your salesforce account's DevHub

    ```
    sfdx auth:web:login -d -a DevHub
    ```

8. Create a new scratch org

    ```
    sfdx force:org:create -s -f config/project-scratch-def.json -a jansahas-org 
    ```

9. Open your scratch org for testing

    ```
    sfdx force:org:open 
    ```

10. Push local changes to new scratch org

    ```
    sfdx force:org:push
    ```


11. Command to create a scratch org with a 30 day expiration

    ```
    sfdx force:org:create -f config/project-scratch-def.json –durationdays 30 -a <Set-Alias>
    ```

12. Steps to deploy to prod
	a. Login to prod account via cli: sfdx auth:web:login -a jansahas-prod

	b. Deploy: sfdx force:source:deploy -p "force-app\main" -l RunLocalTests -u jansahas-prod